# Task Runner #

API Server for task queue application based on Python and Flask

### Requirements ###
* Python 2.7
* [MongoDB](https://docs.mongodb.com/manual/administration/install-on-linux/) - default installation 

### Installation ###

* Install [Werkzeug](http://werkzeug.pocoo.org/docs/0.11/installation/#installing-a-released-version) 
* Install [Jinja2](http://jinja.pocoo.org/docs/2.9/) 
* Install [Mongoengine](http://docs.mongoengine.org/)
* If you need to change Mongo host/port - edit application.py
* OPTIONAL, but recommended - populate database with test data - run it from root directory 

```
#!python

curl -X POST -d @bulk.json -H 'Content-type: application/json' http://localhost:3001/bulk 
```


### Run ###

```
#!python

python application.py
```

### API Calls ###
* Add new task

```
#!python

curl -X POST -d '{"priority":3,"name":"Test","description":"Test"}' -H 'Content-type: application/json' http://localhost:3001/tasks

```
* Get list of tasks

```
#!python

curl '127.0.0.1:3001/list?offset=1&limit=5&order=-created'
```

* Remove Task

```
#!python

curl -X POST -d '{"id": "58e169ef4b8b5408bfba22af"}' -H 'Content-type: application/json' '127.0.0.1:3001/remove'
```