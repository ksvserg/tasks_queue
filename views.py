from werkzeug.exceptions import NotFound
from utils import render_template, expose, ResponseApp as Response
from models import Task
import simplejson as json
from mongoengine import DoesNotExist
from mongoengine.queryset.visitor import Q

# save single task
@expose('/tasks')
def tasks(request):
    if request.method == "POST":
        task_json = json.loads(request.data)
        try:
            t = Task.objects.get(name = task_json["name"])
            t.description = task_json["description"]
            t.priority = task_json["priority"]
        except DoesNotExist: 
            t = Task(name = task_json["name"], description = task_json["description"], priority = task_json["priority"])
        t.save()
        return Response(t.id)
        
    return Response('OK')

# bulk task saving
@expose('/bulk')
def bulk(request):
    if request.method == "POST":
        tasks = json.loads(request.data)
        for task in tasks:
            try:
                t = Task.objects.get(name = task["name"])
                t.description = task["description"]
                t.priority = task["priority"]
            except DoesNotExist: 
                t = Task(name = task["name"], description = task["description"], priority = task["priority"])
            t.save()
        return Response('Done')
        
    return Response('OK')

# get list of tasks
@expose('/list')
def list(request):
    tasks = []
    order = request.args.get('order', '-created');
    limit = int(request.args.get('limit', 2));
    offset = int(request.args.get('offset', 0));
    #filter params
    name = request.args.get('name');
    description = request.args.get('description');
    priority = request.args.get('priority');
    query = Q()
    if (name is not None): query &= Q(name__icontains = name)
    if (description is not None): query &= Q(description__icontains = description)
    if ((priority is not None) & (priority != '')): query &= Q(priority__icontains = priority)
    
    hasMore = Task.objects(query).count() > (offset + limit);
    for task in Task.objects(query).order_by(order).skip(offset).limit(limit):
        tasks.append(json.loads(task.to_json()))
    return Response(json.dumps({ 'list': tasks, 'hasMore': hasMore }))

#remove single task
@expose('/remove')
def remove(request):
    if request.method == "POST":
        task_json = json.loads(request.data)
        t = Task.objects.get(id = task_json["id"])
        t.delete()
        return Response('OK')
        
    return Response('OK')
    
def not_found(request):
    return render_template('not_found.html')
