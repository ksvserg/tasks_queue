from mongoengine import *
import datetime

class Task(Document):
    name = StringField(max_length=200, required=True, unique=True)
    description = StringField(max_length=2000)
    priority = IntField()
    created = DateTimeField(default=datetime.datetime.now)
