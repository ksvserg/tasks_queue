from os import path
from random import sample, randrange
from jinja2 import Environment, FileSystemLoader
from werkzeug.local import Local, LocalManager
from werkzeug.utils import cached_property
from werkzeug.wrappers import Response
from werkzeug.routing import Map, Rule


TEMPLATE_PATH = path.join(path.dirname(__file__), 'templates')
STATIC_PATH = path.join(path.dirname(__file__), 'static')
ALLOWED_SCHEMES = frozenset(['http', 'https', 'ftp', 'ftps'])
URL_CHARS = 'abcdefghijkmpqrstuvwxyzABCDEFGHIJKLMNPQRST23456789'

local = Local()
local_manager = LocalManager([local])
application = local('application')

url_map = Map([Rule('/static/<file>', endpoint='static', build_only=True)])
jinja_env = Environment(loader=FileSystemLoader(TEMPLATE_PATH))

class ResponseApp(Response):
    def __init__(self, resp, **kw):
        Response.__init__(self, resp, **kw)
        self.headers.add('Access-Control-Allow-Origin', '*')
        self.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS')
        self.headers.add('Access-Control-Allow-Headers', 'Content-Type, Authorization')
        
def expose(rule, **kw):
    def decorate(f):
        kw['endpoint'] = f.__name__
        url_map.add(Rule(rule, **kw))
        return f
    return decorate

def render_template(template, **context):
    return ResponseApp(jinja_env.get_template(template).render(**context),
                    mimetype='text/html')

def get_random_uid():
    return ''.join(sample(URL_CHARS, randrange(3, 9)))
